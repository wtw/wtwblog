from django.contrib.auth.models import User
from rest_framework import serializers
from blogapp.models import Comment, LikeComment, LikePost, Post


def validate_current_user_id(current_user, target_user):
    if current_user != target_user:
        # raise serializers.ValidationError("Only the author of this comment can update or delete it!")
        raise serializers.ValidationError(
            "Naughty naughty youuuu teasinggg meee!! Trying to change the user ehh!")
    return


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = '__all__'

    def validate_author(self, value):
        if User.objects.filter(id=value).count() == 0:
            raise serializers.ValidationError(
                "Invalid User id {}".format(value))
        return value

    def validate_post_author(self, post_user_id, user):
        if post_user_id != user.id:
            # raise serializers.ValidationError("Only the author of this post can update or delete it!")
            raise serializers.ValidationError(
                "Naughty naughty youuuu teasinggg meee!! Trying to change my post ehh! ENAAAADIII")
        return


class CommentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Comment
        fields = '__all__'

    def validate_user(self, value):
        if User.objects.filter(id=value).count() == 0:
            raise serializers.ValidationError(
                "Invalid User id {}".format(value))
        return value

    def validate_comment_user_post(self, comment, user, post):
        if comment.user != user.id:
            # raise serializers.ValidationError("Only the author of this comment can update or delete it!")
            raise serializers.ValidationError(
                "Naughty naughty youuuu teasinggg meee!! Trying to change the user ehh!")
        elif comment.post.id != post:
            raise serializers.ValidationError(
                "Naughty naughty youuuu teasinggg meee!! Trying to change my comment ehh! ENAAAADIII")
        return


class LikePostSerializer(serializers.ModelSerializer):

    class Meta:
        model = LikePost
        fields = '__all__'


class LikeCommentSerializer(serializers.ModelSerializer):

    class Meta:
        model = LikeComment
        fields = '__all__'
