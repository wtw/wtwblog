from rest_framework import routers
from .api import CommentViewSet, PostViewSet

router = routers.DefaultRouter()
router.register('blogapi/post', PostViewSet, 'posts')
router.register('blogapi/comment', CommentViewSet, 'comments')
urlpatterns = router.urls
