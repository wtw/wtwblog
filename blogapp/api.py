from rest_framework import permissions, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from blogapp.models import Comment, LikeComment, LikePost, Post
from .serializers import (CommentSerializer, LikeCommentSerializer,
                          LikePostSerializer, PostSerializer,
                          validate_current_user_id)


# Post Viewset
class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    permission_classes = [
        permissions.IsAuthenticated
    ]
    serializer_class = PostSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validate_current_user_id(request.data['author'], request.user.id)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(
            instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        serializer.validate_post_author(request.data['author'], request.user)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        partial = kwargs.pop('partial', False)
        serializer = self.get_serializer(
            instance, data=request.data, partial=partial)
        serializer.validate_post_author(instance, request.user)
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(detail=True, methods=['GET'], name='Like a post')
    def like_post(self, request, pk=None):
        if Post.objects.filter(id=pk).count() > 0:
            like_object = LikePost.objects.filter(
                user=request.user.id, post_id=pk)
            post = Post.objects.get(id=pk)
            if like_object.count() == 0:
                str = "liked"
                like_object = LikePost.objects.create(
                    user=request.user.id, post_id=pk)
                like_object.save()
                post.likes = post.likes+1
                post.save()

            else:
                str = "unliked"
                like_object.delete()
                if post.likes - 1 >= 0:
                    post.likes = (post.likes - 1)
                post.save()
            return Response(
                "Post id {} {} by user id {} name {}".format(
                    pk, str, request.user.id, request.user.username),
                status=status.HTTP_201_CREATED)
        return Response("Invalid Post ID", status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class CommentViewSet(viewsets.ModelViewSet):
    queryset = Comment.objects.all()
    permission_classes = [
        permissions.IsAuthenticated
    ]
    serializer_class = CommentSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validate_current_user_id(request.data['user'], request.user.id)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(
            instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        serializer.validate_comment_user_post(
            instance, request.user, int(request.data['post']))
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        partial = kwargs.pop('partial', False)
        serializer = self.get_serializer(
            instance, data=request.data, partial=partial)
        serializer.validate_comment_user_post(
            instance, request.user, int(request.data['post']))
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(detail=True, methods=['GET'], name='Get comments by post')
    def comments_by_post(self, request, pk=None):
        if Post.objects.filter(id=pk).count() > 0:
            queryset = Comment.objects.filter(post_id=pk)
            serializer = self.get_serializer(queryset, many=True)
            return Response(serializer.data)
        return Response("Invalid Post ID", status=status.HTTP_404_NOT_FOUND)

    @action(detail=True, methods=['GET'], name='Like a comment')
    def like_comment(self, request, pk=None):
        if Comment.objects.filter(id=pk).count() > 0:
            comment_object = LikeComment.objects.filter(
                user=request.user.id, comment_id=pk)
            comment = Comment.objects.get(id=pk)
            if comment_object.count() == 0:
                str = "liked"
                comment_object = LikeComment.objects.create(
                    user=request.user.id, comment_id=pk)
                comment_object.save()
                comment.likes = comment.likes + 1
                comment.save()
            else:
                str = "unliked"
                comment_object.delete()
                if comment.likes - 1 >= 0:
                    comment.likes = comment.likes - 1
                comment.save()
            return Response("Comment id {} {} by user id {} name {}".format(pk, str, request.user.id, request.user.username), status=status.HTTP_201_CREATED)
        return Response("Invalid Comment ID", status=status.HTTP_500_INTERNAL_SERVER_ERROR)
