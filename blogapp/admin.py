from django.contrib import admin
from .models import Comment, Post

# Register your models here.


class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'created_on')
    search_fields = ['title', 'content']

    def has_change_permission(self, request, obj=None):
        opts = self.opts
        if isinstance(obj, Post):
            if request.user == obj.author:
                return True
            return False
        return admin.ModelAdmin.has_change_permission(self, request, obj)

    def has_delete_permission(self, request, obj=None):
        opts = self.opts
        if isinstance(obj, Post):
            if request.user == obj.author:
                return True
            return False
        return admin.ModelAdmin.has_delete_permission(self, request, obj)


class CommentAdmin(admin.ModelAdmin):
    list_display = ('name', 'body', 'created_on')
    search_fields = ['title', 'body']

    def has_change_permission(self, request, obj=None):
        if isinstance(obj, Comment):
            if request.user == obj.user:
                return True
            return False
        return admin.ModelAdmin.has_change_permission(self, request, obj)

    def has_delete_permission(self, request, obj=None):
        if isinstance(obj, Comment):
            if request.user == obj.user:
                return True
            return False
        return admin.ModelAdmin.has_delete_permission(self, request, obj)


admin.site.register(Post, PostAdmin)
admin.site.register(Comment, CommentAdmin)
