from django.contrib.auth.models import User
from django.db import models

# Create your models here.


class Post(models.Model):
    title = models.CharField(max_length=200, unique=True)
    author = models.IntegerField()
    updated_on = models.DateTimeField(auto_now=True)
    content = models.TextField()
    likes = models.IntegerField(default=0)
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created_on']

    def get_user(self):
        return User.objects.get(id=self.author)

    def set_user(self, user):
        self.author = user.id

    def __str__(self):
        return self.title


class Comment(models.Model):
    post = models.ForeignKey(
        Post, on_delete=models.CASCADE, related_name='comments')
    user = models.IntegerField()
    name = models.CharField(max_length=80)
    email = models.EmailField(max_length=100, unique=False)
    body = models.TextField()
    likes = models.IntegerField(default=0)
    created_on = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=False)

    class Meta:
        ordering = ['created_on']

    def get_user(self):
        return User.objects.get(id=self.user)

    def set_user(self, user):
        self.user = user.id

    def __str__(self):
        return 'Comment {} by {} on {}'.format(self.body, self.name, self.created_on)

class LikePost(models.Model):
    user = models.IntegerField()
    post = models.ForeignKey(
        Post, on_delete=models.CASCADE, related_name="posts_likes")

    def __str__(self):
        return f"{self.user} liked {self.post}"

    def get_user(self):
        return User.objects.get(id=self.user)

    def set_user(self, user):
        self.user = user.id


class LikeComment(models.Model):
    user = models.IntegerField()
    comment = models.ForeignKey(
        Comment, on_delete=models.CASCADE, related_name="comments_likes")

    def __str__(self):
        return f"{self.user} liked {self.comment}"

    def get_user(self):
        return User.objects.get(id=self.user)

    def set_user(self, user):
        self.user = user.id
